
import dto.response.books.BookResponseDTO;
import org.testng.annotations.Test;
import steps.BookSteps;
import static org.assertj.core.api.Assertions.assertThat;
public class GetBookTest {
    /*
     * Написать сценарий, в котором вы будете получать книги из запроса /BookStore/v1/books,
     * далее выберете одну какую-то книгу, затем через стрим отфильтровать книги по title,
     * и вывести ту самую книгу, но не всю, а лишь ее isbn.
     * Далее, в запросе /BookStore/v1/Book задать в параметр то,что уже получили, и вернуть книгу,
     * проверив через ассерт, что книга действительно та.
     * Обратите внимание на ендпоинт получения книги (одной), там параметр передается по-другому,
     * а именно - /BookStore/v1/Book?ISBN={isbn}
     *
     * */

    @Test
    void assertBook(){
        // Get any book
        String selectedISBN=getOneBook();
        // Get selected book by ISBN
        BookResponseDTO response = new BookSteps().getBookByISBN(selectedISBN);
        assertThat(response.getIsbn().equals(selectedISBN));
    }
    String getOneBook() {
        String selectedISBN=null;
        var books = new BookSteps().getAllBooks().getBooks();

        Boolean isSelectedTitle=books
                .stream()
                .findFirst()
                .filter(book->book.getTitle().isEmpty())
                .isPresent();

        if (!isSelectedTitle)
            selectedISBN=books.stream()
                    .findFirst()
                    .filter(book->book.getTitle()
                    .equals("Git Pocket Guide")).get().getIsbn();

    return selectedISBN;

    }
}







