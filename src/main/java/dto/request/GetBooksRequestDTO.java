package dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetBooksRequestDTO {
    private String isbn,
            title,
            subTitle,
            author,
            publisher,
            description,
            website;
    private int pages;
    private Date publish_date;

}
